package com.rooftopworks.example.mouserobot.core

/**
 * An dapter capaple of converting between a type [T] and [Move]
 */
interface MoveAdapter<T> {
    fun toMoves(move: T): List<Move>
}