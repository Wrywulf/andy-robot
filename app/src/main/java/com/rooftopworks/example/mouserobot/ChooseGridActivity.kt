package com.rooftopworks.example.mouserobot

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity

import kotlinx.android.synthetic.main.activity_choose_grid.*
import kotlinx.android.synthetic.main.content_choose_grid.*

class ChooseGridActivity : AppCompatActivity() {

  companion object {
    const val BUNDLE_KEY_GRID_ROWS = "grid_rows"
    const val BUNDLE_KEY_GRID_COLUMN = "grid_columns"
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_choose_grid)
    setSupportActionBar(toolbar)

    fab.setOnClickListener { view ->
      if (rows.text.isNotEmpty() && columns.text.isNotEmpty()) {
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra(BUNDLE_KEY_GRID_ROWS, Integer.parseInt(rows.text.toString()))
        intent.putExtra(BUNDLE_KEY_GRID_COLUMN, Integer.parseInt(columns.text.toString()))
        startActivity(intent)
      } else {
        Snackbar.make(view, "Enter both row and column to continue.", Snackbar.LENGTH_SHORT).show()
      }
    }
  }
}
