package com.rooftopworks.example.mouserobot.adapter

import com.rooftopworks.example.mouserobot.core.Move
import com.rooftopworks.example.mouserobot.core.MoveAdapter
import timber.log.Timber

/**
 * Adapts from a string of "L", "R", "F" to a list of [Move]
 */
class TextMoveAdapter : MoveAdapter<String> {
  override fun toMoves(move: String): List<Move> {
    //TODO Validate input string
    val stringMoves: List<String> = move.toUpperCase().split("")
    val moves = mutableListOf<Move>()

    for (s: String in stringMoves) {
      try {
        moves.add(parseMove(s))
      } catch (e: IllegalArgumentException) {
                // Just skip it
      }
    }
    return moves
  }

  private fun parseMove(value: String): Move {
    return when (value) {
      "L" -> Move.LeftRotate
      "R" -> Move.RightRotate
      "F" -> Move.Forward
      else -> {
        throw IllegalArgumentException("Cannot parse $value into a Move")
      }
    }
  }
}