package com.rooftopworks.example.mouserobot.core

enum class Move(type: kotlin.String) {
    LeftRotate("LeftRotate"),
    RightRotate("RightRotate"),
    Forward("Forward");
}