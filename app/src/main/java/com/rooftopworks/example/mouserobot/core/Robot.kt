package com.rooftopworks.example.mouserobot.core

import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import timber.log.Timber

/**
 * A robot capable of rotatating itself and moving forward through a [Grid] based on [Move]
 * inputs provided by a [RobotController]. The robot has a starting position [position] provided
 * in constructor
 */
class Robot(position: Position = Position.create(1, 1, Position.NORTH),
            val grid: Grid,
            controller: RobotController) {
  val position: Position
    get() {
      return positionRelay.value
    }

  private val disposable = CompositeDisposable()

  private val positionRelay: BehaviorRelay<Position> = BehaviorRelay.createDefault(position)

  private val positionStream: Observable<Position> = Observable.zip(controller.controlInput(),
                                                                    positionRelay,
                                                                    BiFunction { move: Move, oldPosition: Position ->
                                                                      Pair(oldPosition,
                                                                           move)
                                                                    })
          .doOnNext {
            (_, move) ->
            Timber.d("Moving %s", move)
          }
          .map { (first, second) -> grid.move(first, second) }


  init {
    if (!grid.isWithinGrid(position)) {
      throw IllegalArgumentException("Starting position $position is not within grid $grid")
    }
  }

  /**
   * Turns on Robot which means that it reacts to control input
   */
  fun turnOn() {
    disposable.add(positionStream.subscribe({ newPosition ->
                                              Timber.d("Robot position: %s", newPosition)
                                              positionRelay.accept(newPosition)
                                            }))
  }

  /**
   * Turns of Robot which means that it does no longer react to control input
   */
  fun turnOff() {
    disposable.clear()
  }

  /**
   * Emits a boolean for every position change and an initial value upon subscription. Emits true
   * if able to move forward, false if not
   */
  fun canMoveForwardStream(): Observable<Boolean> {
    return positionRelay
            .map { grid.canMoveForward(position) }
  }

  /**
   * Emits the current (latest) position upon subscription and then subsequent changes
   */
  fun positionStream(): Observable<Position> {
    return positionRelay
  }
}