package com.rooftopworks.example.mouserobot.widget

import android.content.Context
import android.support.annotation.LayoutRes
import android.support.annotation.Nullable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.GridLayout
import android.widget.TextView
import timber.log.Timber

/**
 * A [GridLayout] which stretches or shrinks its children to fit inside it
 */
class BoardGrid : GridLayout {
  constructor(context: Context) : this(context, null)
  constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

  constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context,
                                                                                 attrs,
                                                                                 defStyleAttr)

  /**
   * Initialize the board grid with number of rows/columns, a layout resource ID to inflate
   * (expecting a
   * [TextView] for now, will throw if not) and an [View.OnClickListener]
   */
  fun setGrid(rows: Int, columns: Int, @LayoutRes viewResId: Int, clickListener:
  OnClickListener?) {
    removeAllViews()
    rowCount = rows
    columnCount = columns

    var r = rows
    while (r >= 1) { // run "inverse" to get 1,1 origin at lower left corner of gridlayout
      var c = 1
      while (c <= columns) {
        val view = createView(viewResId)
        // Create the GridLayout.Spec defining:
        //  (a) the row/column start indice (0-indexed)
        //  (b) each view take 1 column/row span
        //  (c) each view should fill it boundaries horizontally and vertically (FILL)
        //  (d) each view's boundaries should take up as much space as possible, equally distributed
        val params = view.layoutParams as GridLayout.LayoutParams

        params.rowSpec = spec(rows-r, 1, FILL, 1f)
        params.columnSpec = spec(c-1, 1, FILL, 1f)

        params.width = 1
        params.height = 1
        view.layoutParams = params
        view.tag = GridPosition(c, r) // row: y-axis, column: x-axis so invert here
        view.setOnClickListener(clickListener)
        addView(view)
        c++
      }
      r--
    }
  }

  private fun createView(@LayoutRes viewResId: Int): View {
    val layoutInflater = LayoutInflater.from(context)
    return layoutInflater.inflate(viewResId, this, false)
  }

  fun getViewAt(x: Int, y: Int) : View? {
    val coord = GridPosition(x, y)
    var index = 0
    while (index < childCount) {
      val v = getChildAt(index)
      if (v.tag == coord) {
        Timber.d("Found view $v @ $x,$y")
        return v
      }
      index++
    }
    return null
  }

  data class GridPosition(val x: Int, val y: Int)
}