package com.rooftopworks.example.mouserobot

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.jakewharton.rxbinding2.view.RxView
import com.rooftopworks.example.mouserobot.adapter.TextMoveAdapter
import com.rooftopworks.example.mouserobot.controller.TextRobotController
import com.rooftopworks.example.mouserobot.core.Grid
import com.rooftopworks.example.mouserobot.core.Position
import com.rooftopworks.example.mouserobot.core.Robot
import com.rooftopworks.example.mouserobot.core.RobotController
import com.rooftopworks.example.mouserobot.widget.BoardGrid
import com.rooftopworks.example.mouserobot.widget.RobotView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.content_main.*
import timber.log.Timber
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

  lateinit var robot: Robot

  /** It's getting late and this code ended up a bit messier than it should have*/
  //TODO separate into viewmodel / view
  @SuppressLint("SetTextI18n")
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    val robotController = TextRobotController(TextMoveAdapter(), RxView.clicks(move).map {
      movesInput.text.toString()
    })

    val rows = intent.getIntExtra(ChooseGridActivity.BUNDLE_KEY_GRID_ROWS, 5)
    val columns = intent.getIntExtra(ChooseGridActivity.BUNDLE_KEY_GRID_COLUMN, 5)

    // input controller shortcuts (as opposed to typing commands)
    rotateLeft.setOnClickListener { movesInput.append("L") }
    rotateRight.setOnClickListener { movesInput.append("R") }
    forward.setOnClickListener { movesInput.append("F") }

    if (savedInstanceState != null) {
      Timber.d("Restoring state: %s", savedInstanceState)
      boardGrid.setGrid(rows, columns, R.layout.grid_cell, null)
      val x = savedInstanceState.getInt("robot.position.x")
      val y = savedInstanceState.getInt("robot.position.y")
      val heading = savedInstanceState.getInt("robot.position.heading")
      // post this to let the grid be created first
      Handler().post({
                       createRobot(BoardGrid.GridPosition(x, y),
                                   heading,
                                   rows,
                                   columns,
                                   robotController)
                     })
    } else {
      var waitingForInput: Boolean = true
      Snackbar.make(boardGrid, "Pick a starting point for the Robot.", Snackbar
              .LENGTH_LONG).show()
      // build grid layout
      boardGrid.setGrid(rows, columns, R.layout.grid_cell, View.OnClickListener { view ->
        Timber.d("View with tag %s clicked", view.tag)
        if (waitingForInput) {
          waitingForInput = false
          val gridPosition = view.tag as BoardGrid.GridPosition
          createRobot(gridPosition, Position.NORTH, rows, columns, robotController)
        }
      })
    }
  }

  private fun createRobot(gridPosition: BoardGrid.GridPosition, heading: Int, rows: Int,
                          columns:
                          Int,
                          robotController: RobotController) {
    // create Robot
    robot = Robot(Position.create(gridPosition.x, gridPosition.y, heading),
                  Grid(rows, columns),
                  robotController)

    // create robot UI
    val rV = layoutInflater.inflate(R.layout.robot_view, gridContainer, false) as RobotView
    rV.initialize(robot, boardGrid)
    gridContainer.addView(rV)

    // track its movement in text as well
    trackMovementAsText(robot)

    robot.turnOn()
  }

  override fun onSaveInstanceState(outState: Bundle?) {
    outState?.putInt("robot.position.x", robot.position.x)
    outState?.putInt("robot.position.y", robot.position.y)
    outState?.putInt("robot.position.heading", robot.position.heading)
    super.onSaveInstanceState(outState)
  }

  @SuppressLint("SetTextI18n")
  private fun trackMovementAsText(robot: Robot) {
    robot.positionStream()
            // introduce same delays as in RobotView, just for the sake of it
            .concatMap { position ->
              Observable.interval(RobotView.MOVE_STEP_DURATION_MS,
                                  RobotView.MOVE_STEP_DURATION_MS,
                                  TimeUnit.MILLISECONDS).take(1).map({ position })
            }
            .observeOn(AndroidSchedulers.mainThread()).subscribe({ pos ->
                                                                   textPosition.text = "Position: ${pos.x} ${pos.y} ${displayHeading(
                                                                           pos.heading)}"
                                                                 })
  }

  private fun displayHeading(heading: Int): String {
    return when (heading) {
      0 -> "N"
      90 -> "E"
      180 -> "S"
      270 -> "W"
      else -> {
        heading.toString()
      }
    }
  }
}
