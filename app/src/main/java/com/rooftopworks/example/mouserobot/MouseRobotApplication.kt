package com.rooftopworks.example.mouserobot

import android.app.Application
import timber.log.Timber

/**
 * TODO: make javadoc
 */
class MouseRobotApplication : Application() {

  override fun onCreate() {
    super.onCreate()
    if (BuildConfig.DEBUG) {
      Timber.plant(Timber.DebugTree())
    }
  }
}