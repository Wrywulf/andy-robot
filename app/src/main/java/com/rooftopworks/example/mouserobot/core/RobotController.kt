package com.rooftopworks.example.mouserobot.core

import io.reactivex.Observable

/**
 * TODO: make javadoc
 */
interface RobotController {

    fun controlInput(): Observable<Move>
}