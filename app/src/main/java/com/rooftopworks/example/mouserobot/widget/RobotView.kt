package com.rooftopworks.example.mouserobot.widget

import android.content.Context
import android.support.v7.widget.AppCompatImageView
import android.util.AttributeSet
import android.view.View
import at.wirecube.additiveanimations.additive_animator.AdditiveAnimator
import com.rooftopworks.example.mouserobot.core.Position
import com.rooftopworks.example.mouserobot.core.Robot
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import timber.log.Timber
import java.util.concurrent.TimeUnit

/**
 * An android [View] that tracks the [Robot]
 */
class RobotView : AppCompatImageView {
  constructor(context: Context) : this(context, null)
  constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

  constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context,
                                                                                 attrs,
                                                                                 defStyleAttr)
  var View.centerX: Float
    get() {
      return x + width / 2
    }
    set(value) {
      x = value - width / 2
    }

  var View.centerY: Float
    get() {
      return y + height / 2
    }
    set(value) {
      y = value - height / 2
    }

  companion object {
    val MOVE_STEP_DURATION_MS = 300L
  }

  fun initialize(robot: Robot, boardGrid: BoardGrid) {
    // set initial position and size synchronously
    val gridCell = boardGrid.getViewAt(robot.position.x,
                                       robot.position.y) ?: throw IllegalStateException("Position ${robot.position} does not exist in grid")
    Timber.d("Starting with Grid cell: x: ${gridCell.x} y: ${gridCell.y} centerX: ${gridCell
            .centerX}, centerY: ${gridCell.centerY}")

    rotation = robot.position.heading.toFloat()
    resizeAndPositionIfNeeded(gridCell)

    robot.positionStream()
            // introduce delays when setting animation targets to illustrate complete path movement
            // and not just "skipping" to end position
            .concatMap { position ->
              Observable.interval(MOVE_STEP_DURATION_MS,
                                  0L,
                                  TimeUnit.MILLISECONDS).take(1).map({ position })
            }
            .observeOn(AndroidSchedulers.mainThread()).subscribe({ position ->
                                                                       moveToPosition(
                                                                               boardGrid,
                                                                               position)
                                                                 })
  }

  override fun postInvalidateOnAnimation() {
    super.postInvalidateOnAnimation()
  }

  private fun moveToPosition(boardGrid: BoardGrid,
                             position: Position) {
    val gridCell = boardGrid.getViewAt(position.x,
                                       position.y) ?: throw IllegalStateException("Position ${position} does not exist in grid")
    // animate view movement
    AdditiveAnimator.animate(this).centerX(gridCell.centerX).centerY(gridCell.centerY).rotation(
            position.heading.toFloat()).setDuration(MOVE_STEP_DURATION_MS).start()

    Timber.d("Moving to Grid cell: centerX: ${gridCell.centerX}, centerY: ${gridCell.centerY}")
  }

  private var initialized: Boolean = false

  private fun resizeAndPositionIfNeeded(cell: View) {
    if (!initialized) {
      val maxSide = Math.min(cell.width, cell.height) * 0.8 // magic ratio for now
      val aspectRatio = drawable.intrinsicWidth.toFloat() / drawable.intrinsicHeight.toFloat()

      val params = layoutParams
      if (drawable.intrinsicWidth > drawable.intrinsicHeight) {
        params.width = maxSide.toInt()
        params.height = (maxSide / aspectRatio).toInt()
      } else {
        params.width = (maxSide / aspectRatio).toInt()
        params.height = maxSide.toInt()
      }
      layoutParams = params

      // cannot use centerX/Y props yet, because our width is not yet set. So set center manually
      // in cell
      x += cell.x + (cell.width - params.width) / 2
      y += cell.y + (cell.height - params.height) / 2

      initialized = true
    }
  }
}
