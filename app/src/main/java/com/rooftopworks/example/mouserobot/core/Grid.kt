package com.rooftopworks.example.mouserobot.core

import io.reactivex.Observable


/**
 * A representation of a rectangular grid with origin (1,1) in south-west corner
 */
class Grid(val rows: Int, val columns: Int) {

    fun isWithinGrid(pos: Position) : Boolean {
        return pos.x >= 1 && pos.x <= columns && pos.y >= 1 && pos.y <= rows;
    }

    fun canMoveForward(pos: Position): Boolean {
        return when (pos.heading) {
            Position.NORTH -> pos.y < rows
            Position.EAST -> pos.x < columns
            Position.SOUTH -> pos.y > 1
            Position.WEST -> pos.x > 1
            else -> false
        }
    }

    private fun moveForward(pos: Position): Position {
        return when (pos.heading) {
            Position.NORTH -> pos.copy(y = pos.y + 1)
            Position.EAST -> pos.copy(x = pos.x + 1)
            Position.SOUTH -> pos.copy(y = pos.y - 1)
            Position.WEST -> pos.copy(x = pos.x - 1)
            else -> pos
        }
    }

    fun move(pos: Position, move: Move): Position {
        return when (move) {
            Move.LeftRotate -> pos.rotate(-90)
            Move.RightRotate -> pos.rotate(90)
            Move.Forward -> {
                if (canMoveForward(pos)) {
                    moveForward(pos)
                } else {
                    pos
                }
            }
        }
    }

    fun move(input: Observable<Pair<Position, Move>>): Observable<Position> {
        return input.map { pair -> move(pair.first, pair.second) }
    }
}

