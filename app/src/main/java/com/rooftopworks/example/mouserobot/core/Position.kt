package com.rooftopworks.example.mouserobot.core

/**
 * A representation of a discrete position in a grid, with a compass heading (degrees)
 */
data class Position private constructor(val x: Int, val y: Int, val heading: Int) {



    /**
     * Returns a new [Position] instance, which is rotated (has a new heading) based on the given relative rotation in degrees
     * @param degrees rotation angle. Should be in range -360..360. If not, it will be mapped to that range
     */
    fun rotate(degrees: Int): Position {
        val newHeading = normalizeAngle(heading + degrees)
        return copy(heading = newHeading)
    }


    companion object Utils {
        const val NORTH: Int = 0
        const val EAST: Int = 90
        const val SOUTH: Int = 180
        const val WEST: Int = 270

        /**
         * Use this factory method when instantiating a [Position] because it validates input
         */
        fun create(x: Int, y: Int, heading: Int): Position {
            return Position(x, y, normalizeAngle(heading))
        }
        /**
         * Normalizes an angle in degrees to [0..359] range. Also handles negative angles.
         */
        fun normalizeAngle(angle: Int): Int {
            val modAngle: Int = angle % 360
            return if (modAngle >= 0) modAngle else modAngle + 360
        }
    }

    override fun toString(): String {
        return "$x $y $heading"
    }
}
