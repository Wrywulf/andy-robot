package com.rooftopworks.example.mouserobot.controller

import com.rooftopworks.example.mouserobot.adapter.TextMoveAdapter
import com.rooftopworks.example.mouserobot.core.Move
import com.rooftopworks.example.mouserobot.core.RobotController
import io.reactivex.Observable

/**
 * TODO: make javadoc
 */
class TextRobotController(val adapter: TextMoveAdapter, val stringInput: Observable<String>) :
        RobotController {
  override fun controlInput(): Observable<Move> {
    return stringInput.map { text -> adapter.toMoves(text) }.concatMap { moves ->
      Observable.fromIterable(moves)
    }
  }
}