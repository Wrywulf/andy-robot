package com.rooftopworks.example.mouserobot.core

import org.junit.Test
import org.assertj.core.api.Assertions.*

class GridTest {
    @Test
    fun canMoveForward_evaluatesCorrectly() {
        val grid = Grid(3, 3)
        // vertical north
        assertThat(grid.canMoveForward(Position.create(1, 1, Position.NORTH))).isTrue()
        assertThat(grid.canMoveForward(Position.create(1, 2, Position.NORTH))).isTrue()
        assertThat(grid.canMoveForward(Position.create(1, 3, Position.NORTH))).isFalse()

        // vertical south
        assertThat(grid.canMoveForward(Position.create(1, 3, Position.SOUTH))).isTrue()
        assertThat(grid.canMoveForward(Position.create(1, 2, Position.SOUTH))).isTrue()
        assertThat(grid.canMoveForward(Position.create(1, 1, Position.SOUTH))).isFalse()

        // horizontal east
        assertThat(grid.canMoveForward(Position.create(1, 1, Position.EAST))).isTrue()
        assertThat(grid.canMoveForward(Position.create(2, 1, Position.EAST))).isTrue()
        assertThat(grid.canMoveForward(Position.create(3, 1, Position.EAST))).isFalse()

        // horizontal west
        assertThat(grid.canMoveForward(Position.create(3, 1, Position.WEST))).isTrue()
        assertThat(grid.canMoveForward(Position.create(2, 1, Position.WEST))).isTrue()
        assertThat(grid.canMoveForward(Position.create(1, 1, Position.WEST))).isFalse()

    }

    @Test
    fun translationalMovementPerformed_WhenInsideGrid() {
        val grid = Grid(5, 5)
        assertThat(grid.move(Position.create(1, 1, Position.NORTH), Move.Forward)).isEqualTo(Position.create(1, 2, Position.NORTH))
        assertThat(grid.move(Position.create(1, 1, Position.EAST), Move.Forward)).isEqualTo(Position.create(2, 1, Position.EAST))
        assertThat(grid.move(Position.create(5, 5, Position.SOUTH), Move.Forward)).isEqualTo(Position.create(5, 4, Position.SOUTH))
        assertThat(grid.move(Position.create(5, 5, Position.WEST), Move.Forward)).isEqualTo(Position.create(4, 5, Position.WEST))
    }

    @Test
    fun translationalMovementNoop_WhenOutsideGrid() {
        val grid = Grid(5, 5)
        assertThat(grid.move(Position.create(1, 1, Position.SOUTH), Move.Forward)).isEqualTo(Position.create(1, 1, Position.SOUTH))
        assertThat(grid.move(Position.create(1, 1, Position.WEST), Move.Forward)).isEqualTo(Position.create(1, 1, Position.WEST))
        assertThat(grid.move(Position.create(5, 5, Position.NORTH), Move.Forward)).isEqualTo(Position.create(5, 5, Position.NORTH))
        assertThat(grid.move(Position.create(5, 5, Position.EAST), Move.Forward)).isEqualTo(Position.create(5, 5, Position.EAST))
    }

}