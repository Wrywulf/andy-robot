package com.rooftopworks.example.mouserobot.core

import org.assertj.core.api.Assertions.*
import org.junit.Test


/**
 * TODO: make javadoc
 */
class PositionTest {
    @Test
    fun rotate() {
        assertThat(Position.create(1,1,90).rotate(90).heading).isEqualTo(Position.SOUTH)
        assertThat(Position.create(1,1,90).rotate(270).heading).isEqualTo(Position.NORTH)
        assertThat(Position.create(1,1,90).rotate(-90).heading).isEqualTo(Position.NORTH)
        assertThat(Position.create(1,1,90).rotate(-180).heading).isEqualTo(Position.WEST)
    }

    @Test
    fun normalizeAngle() {
        assertThat(Position.normalizeAngle(-450)).isEqualTo(Position.WEST)
        assertThat(Position.normalizeAngle(-360)).isEqualTo(Position.NORTH)
        assertThat(Position.normalizeAngle(-270)).isEqualTo(Position.EAST)
        assertThat(Position.normalizeAngle(-180)).isEqualTo(Position.SOUTH)
        assertThat(Position.normalizeAngle(-90)).isEqualTo(Position.WEST)
        assertThat(Position.normalizeAngle(0)).isEqualTo(Position.NORTH)
        assertThat(Position.normalizeAngle(90)).isEqualTo(Position.EAST)
        assertThat(Position.normalizeAngle(180)).isEqualTo(Position.SOUTH)
        assertThat(Position.normalizeAngle(270)).isEqualTo(Position.WEST)
        assertThat(Position.normalizeAngle(360)).isEqualTo(Position.NORTH)
        assertThat(Position.normalizeAngle(450)).isEqualTo(Position.EAST)
    }
}