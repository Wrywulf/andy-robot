package com.rooftopworks.example.mouserobot.core

import com.jakewharton.rxrelay2.PublishRelay
import com.rooftopworks.example.mouserobot.controller.TextRobotController
import io.reactivex.BackpressureStrategy
import io.reactivex.Observable
import io.reactivex.functions.Predicate
import org.junit.Test

import org.assertj.core.api.Assertions.*

class RobotTest {

  @Test(expected = IllegalArgumentException::class)
  fun cannotCreateRobot_WhenStartingPositionIsOffGrid() {
    Robot(Position.create(0, 1, Position.NORTH), Grid(5, 5), object : RobotController {
      override fun controlInput(): Observable<Move> {
        return Observable.just(Move.Forward) // Dummy
      }
    })
  }

  @Test
  fun robotEmitsPositionUpdates_WhenMovedWithinGrid() {
    /* Start at 1,1 and attempt to move north 3 times in 10x10 grid. Final position should be 1,4
     and all intermediate positions should be emitted *  */
    val relay: PublishRelay<Move> = PublishRelay.create()
    val robot = Robot(Position.create(1, 1, Position.NORTH),
                      Grid(10, 10),
                      object : RobotController {
                        // a simple controller that emits Moved via a relay (such that it is controllable during test)
                        override fun controlInput(): Observable<Move> {
                          return relay
                        }
                      })

    /* Act */
    robot.turnOn()

    /* Assert */
    // make it a flowable to control the backpressure and to be able to request values when needed
    val testSubscriber = robot.positionStream().toFlowable(BackpressureStrategy.BUFFER).test(0)

    // initial emitted value is current position
    testSubscriber.requestMore(1).assertValues(Position.create(1, 1, Position.NORTH)) //

    relay.accept(Move.Forward)
    relay.accept(Move.Forward)
    relay.accept(Move.Forward)

    testSubscriber.requestMore(3).assertValues(Position.create(1, 1, Position.NORTH), Position
            .create(1, 2, Position.NORTH), Position.create(1, 3, Position.NORTH), Position
                                                       .create(1,
                                                               4,
                                                               Position.NORTH)).assertNotTerminated()

    assertThat(robot.position).isEqualTo(Position.create(1, 4, Position.NORTH))
  }

  @Test
  fun robotPositionRemainsWithinGrid_WhenAttemptedToMoveOffGrid() {
    /* Start at 1,1 and attempt to move north 3 times in 3x3 grid. Final position should be 1,3 */
    val robot = Robot(Position.create(1, 1, Position.NORTH),
                      Grid(3, 3),
                      object : RobotController {
                        // a simple controller that emits Moved via a relay (such that it is controllable during test)
                        override fun controlInput(): Observable<Move> {
                          return Observable.fromIterable(listOf(Move.Forward, Move.Forward, Move
                                  .Forward))
                        }
                      })

    /* Act */
    robot.turnOn()

    /* Assert */
    assertThat(robot.position).isEqualTo(Position.create(1, 3, Position.NORTH))
  }

  @Test
  fun robotCannotMoveForward_WhenAtEdgeOfGrid() {
    /* Start position at 1,1 and face south */
    val robot = Robot(Position.create(1, 1, Position.SOUTH),
                      Grid(3, 3),
                      object : RobotController {
                        // a simple controller that emits Moved via a relay (such that it is controllable during test)
                        override fun controlInput(): Observable<Move> {
                          return Observable.just(Move.Forward)
                        }
                      })

    /* Assert */
    robot.canMoveForwardStream().test()
            .assertValueAt(0, { value -> !value })
            .assertNotTerminated()
  }

  @Test
  fun robotCanMoveForward_WhenDistanceToEdgeIsOneOrMore() {
    /* Start position at 1,1 and face north in 2x2 grid */
    val robot = Robot(Position.create(1, 1, Position.NORTH),
                      Grid(2, 2),
                      object : RobotController {
                        // a simple controller that emits Moved via a relay (such that it is controllable during test)
                        override fun controlInput(): Observable<Move> {
                          return Observable.just(Move.Forward)
                        }
                      })

    /* Assert */
    robot.canMoveForwardStream().test()
            .assertValueAt(0, { value -> value })
            .assertNotTerminated()
  }

}